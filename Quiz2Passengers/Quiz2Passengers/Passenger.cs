﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    class Passenger
    {
        int Id; // INT PK IDENTITY(1,1)
        string Name; // VC(100) NOT NULL
        string PassportNo; // VC(10) NOT NULL
        string Destination; // VC(100) NOT NULL
        DateTime DepartureDateTime; // DateTime type, visible as two inputs in UI
    }
}
