﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum SortOrderEnum { Name, PassNo, Dest, DeptDateTime }

        // PROBABLY NOT NEEDED: List<Passenger> passengerList;

        public SortOrderEnum CurrSortOrder;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void refreshList()
        {
            // fetch all records from database
            // filter if user entered a word into Search text box, best done using LINQ again
            // order them by CurrSortOrder using LINQ
            // give result to ListView as ItemsSource
        }

    }
}
