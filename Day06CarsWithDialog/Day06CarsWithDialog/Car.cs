﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06CarsWithDialog
{
    public class Car
    {
        public Car (string makeModel, double engSizeL, FuelTypeEnum fuelType)
        {
            MakeModel = makeModel;
            EngineSizeL = engSizeL;
            FuelType = fuelType;
        }
        [Name("Make and model")]
        public string MakeModel { get; set; } // 2-50 characters, no semicolons
        [Name("Engine size (L)")]
        public double EngineSizeL { get; set; } // 0-20
        [Name("Fuel type")]
        public FuelTypeEnum FuelType { get; set; }
        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other }
    }
}
