﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07CarDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // List<Car> carsList = new List<Car>();

        public MainWindow()
        {
            InitializeComponent();
            // Globals.Db = new Database(); // ex SqlException
        }

        void refreshListFromDb()
        {
            try
            {
                lvCars.ItemsSource = Globals.Db.GetAllCars(); // ex SqlException, ArgumentException
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Database error:\n" + ex.Message); // FIXME: better dialog
            }
        }
    }
}
