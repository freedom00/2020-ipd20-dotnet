﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07CarDB
{

    class DatabaseException : Exception
    {
        public DatabaseException(string msg, Exception innerException) : base(msg, innerException) { }
    }

    class Database
    {
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Teacher\Documents\2020-ipd20-dotnet\Day07CarDB\CarDB.mdf;Integrated Security=True;Connect Timeout=30";

        SqlConnection conn;

        public Database() // ex SqlException
        {
            conn = new SqlConnection(connString);
            conn.Open();
        }

        public int AddCar(Car car) // ex SqlException
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Cars (MakeModel, EngineSizeL, FuelType) VALUES (@MakeModel, @EngineSizeL, @FuelType); SELECT SCOPE_IDENTITY()", conn);
            cmdInsert.Parameters.AddWithValue("@MakeModel", car.MakeModel);
            cmdInsert.Parameters.AddWithValue("@EngineSizeL", car.EngineSizeL);
            cmdInsert.Parameters.AddWithValue("@FuelType", car.FuelType);
            int id = (int)cmdInsert.ExecuteScalar();
            return id;
        }

        public List<Car> GetAllCars() // ex SqlException, ArgumentException
        {
            List<Car> result = new List<Car>();
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Cars", conn);
            SqlDataReader selectReader = cmdSelectAll.ExecuteReader(); // SqlException
            while (selectReader.Read())
            {
                int id = selectReader.GetInt32(selectReader.GetOrdinal("Id"));
                string makeModel = selectReader.GetString(selectReader.GetOrdinal("MakeModel"));
                double engineSizeL = selectReader.GetDouble(selectReader.GetOrdinal("EngineSizeL"));
                string fuelTypeStr = selectReader.GetString(selectReader.GetOrdinal("FuelType"));
                Car.FuelTypeEnum fuelType = (Car.FuelTypeEnum) Enum.Parse(typeof(Car.FuelTypeEnum), fuelTypeStr); // ex ArgumentException
                Car car = new Car(id, makeModel, engineSizeL, fuelType); // ex ArgumentException
                result.Add(car);
            }
            return result;
        }

        public void UpdateCar(Car car)
        { // id never changes

        }

        public void DeleteCar(int id)
        {

        }

    }
}
