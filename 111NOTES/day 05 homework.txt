DAY 05 HOMEWORK
===============

Day05TravelList
------------------

class Trip {
	string Destination; // 2-30 characters, no semicolons
	string travellerName; // 2-30 characters, no semicolons
	string travellerPassportNo; // two uppercase letters followed by 8 digts
	Date departureDate, returnDate; // year 1900-2100, departure before return
}

To above class add getters and setters with verification.
Also add a constructor that take all 5 parameters.
Also add ToString() to use in dialog boxes.

We do NOT have two setters for departureDate and returnDate but instead ONE:

public void setDepartureAndReturnDates(Date depDate, Date retDate) { }

On failure setters will throw ArgumentException with a message.

For the MainWindow use ListView with GridView to separate all the 5 columns.

Also implement not only Add but Update and Delete as well.

In the MainWindow implement the usual methods and call them when program starts and is about to exit.

void loadDataFromFile()
void saveDataToFile()



PROFILE PHOTOS IN TEAMS
-----------------------

BONUS TASK: After the class try and in Teams how to set your profile photo and set it to a RECENT photo of your face. It will help us make our classroom more personal.


