DAY 04 TASKS
============

Day04ListView
-------------

Read https://www.wpf-tutorial.com/listview-control/listview-data-binding-item-template/

Implement single-window application that allows for basic CRUD operations on:

class Person {
	string Name;
	int Age;
	// TODO: define toString and constructor
}

Use ListView as in the tutorial you've just read.


Day04GridView
-------------

Read https://www.wpf-tutorial.com/listview-control/listview-with-gridview/

class Person {
	string Name {get; set; }
	int Age { get; set; }
	// TODO: define toString and constructor
}

Use ListView with GridView as in the tutorial you've just read.

Adjust the bindings of GridView to match the two *properties* of your class.

Note that these are properties, not fields. Bindings will not work with fields.

