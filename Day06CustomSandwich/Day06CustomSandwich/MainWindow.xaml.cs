﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btMakeSandwich_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog customDialog = new CustomDialog();
            if (customDialog.ShowDialog() == true)
            {
                lblBread.Content = Globals.Bread;
                lblVegetables.Content = Globals.Vegetables;
                lblMeats.Content = Globals.Meat;
            }
        }

        private void btMakeSandwichViaCallback_Click(object sender, RoutedEventArgs e)
        {
            CustomDialogViaCallback customDialog = new CustomDialogViaCallback(this);
            if (customDialog.ShowDialog() == true)
            {
                // nothing to do in this case
            }
        }
        // callback method
        public void setDisplayValues(string bread, string vegetables, string meat)
        {
            lblBread.Content = bread;
            lblVegetables.Content = vegetables;
            lblMeats.Content = meat;
        }

        // STRONGLY PREFERRED SOLUTION - using properties to pass values back
        private void btMakeSandwichViaProperties_Click(object sender, RoutedEventArgs e)
        {
            CustomDialogViaProperties customDialog = new CustomDialogViaProperties();
            if (customDialog.ShowDialog() == true)
            {
                lblBread.Content = customDialog.Bread;
                lblVegetables.Content = customDialog.Vegetables;
                lblMeats.Content = customDialog.Meat;
            }
        }
    }
}
