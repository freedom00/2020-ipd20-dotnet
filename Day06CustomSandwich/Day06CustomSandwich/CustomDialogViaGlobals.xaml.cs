﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        public CustomDialog()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (comboBreadType.SelectedIndex == -1)
            {
                MessageBox.Show("Please select bread type"); // FIXME: must use nicer dialog
                return;
            }
            Globals.Bread = comboBreadType.Text;
            // TODO: find a better way
            List<string> vegList = new List<string>();
            if (cbVegLettuce.IsChecked == true) {
                vegList.Add("Lettuce");
            }
            if (cbVegTomatoes.IsChecked == true)
            {
                vegList.Add("Tomatoes");
            }
            if (cbVegCucumbers.IsChecked == true)
            {
                vegList.Add("Cucumbers");
            }
            Globals.Vegetables = string.Join(", ", vegList);
            string meat;
            if (rbMeatChicken.IsChecked == true)
            {
                meat = "Chicken";
            } else if (rbMeatTurkey.IsChecked == true)
            {
                meat = "Turkey";
            } else if (rbMeatTofu.IsChecked == true)
            {
                meat = "Tofu";
            } else
            {
                MessageBox.Show("Internal error - meat choice problem"); // FIXME: nicer dialog
                return;
            }
            Globals.Meat = meat;
            this.DialogResult = true;
        }
    }
}
