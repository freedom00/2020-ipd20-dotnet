﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day06CustomSandwich
{
    static class Globals
    {
        // Solution 1 (not too elegant): use global variables to pass information around
        public static string Bread, Vegetables, Meat;
        // Usually we used this method for things that need to be accessible for any part of code, such as Database connection
        // Database db; // may even use a Singleton Pattern here, if I want to
    }
}
