﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for CustomDialog.xaml
    /// </summary>
    public partial class CustomDialogViaProperties : Window
    {

        string bread, vegetables, meat;

        public CustomDialogViaProperties()
        {
            InitializeComponent();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (comboBreadType.SelectedIndex == -1)
            {
                MessageBox.Show("Please select bread type"); // FIXME: must use nicer dialog
                return;
            }
            // assign result to private field
            bread = comboBreadType.Text;
            // TODO: find a better way
            List<string> vegList = new List<string>();
            if (cbVegLettuce.IsChecked == true) {
                vegList.Add("Lettuce");
            }
            if (cbVegTomatoes.IsChecked == true)
            {
                vegList.Add("Tomatoes");
            }
            if (cbVegCucumbers.IsChecked == true)
            {
                vegList.Add("Cucumbers");
            }
            // assign result to private field
            vegetables = string.Join(", ", vegList);
            // assign result to private field (in all 3 cases)
            if (rbMeatChicken.IsChecked == true)
            {
                meat = "Chicken";
            } else if (rbMeatTurkey.IsChecked == true)
            {
                meat = "Turkey";
            } else if (rbMeatTofu.IsChecked == true)
            {
                meat = "Tofu";
            } else
            {
                MessageBox.Show("Internal error - meat choice problem"); // FIXME: nicer dialog
                return;
            }
            this.DialogResult = true; // hide dialog
        }

        // read-only properties
        public string Meat
        {
            get
            {
                return meat;
            }
        }

        public string Vegetables
        {
            get
            {
                return vegetables;
            }
        }
        public string Bread
        {
            get
            {
                return bread;
            }
        }
    }
}
