﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08StatesCalc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum CalcStateEnum { Entering1stValue, Entering2ndValue, DisplayingResult }

        CalcStateEnum calcState;

        public MainWindow()
        {
            InitializeComponent();
            resetCalc();
        }

        private void resetCalc()
        {
            calcState = CalcStateEnum.Entering1stValue;
            // clean text boxes, label with operation, etc.
            tbValue1.Text = "";
            tbValue1.IsEnabled = true;
            Keyboard.Focus(tbValue1); // give input 1 focus.
            tbValue2.Text = "";
            tbValue2.IsEnabled = false;
            tbResult.Text = "";
            tbResult.IsEnabled = false;
            lblOperation.Content = "?";

            // later: enable/disable buttons
            setNumButtonsEnabled(true);
            setOpButtonsEnabled(true);
            btResult.IsEnabled = false;
        }

        private void setNumButtonsEnabled(bool isEnabled)
        {
            bt0.IsEnabled = isEnabled;
            bt1.IsEnabled = isEnabled;
            bt2.IsEnabled = isEnabled;
            bt3.IsEnabled = isEnabled;
            bt4.IsEnabled = isEnabled;
            bt5.IsEnabled = isEnabled;
            bt6.IsEnabled = isEnabled;
            bt7.IsEnabled = isEnabled;
            bt8.IsEnabled = isEnabled;
            bt9.IsEnabled = isEnabled;
            btDot.IsEnabled = isEnabled;
            btSignChange.IsEnabled = isEnabled;
        }

        private void setOpButtonsEnabled(bool isEnabled)
        {
            btOpAdd.IsEnabled = isEnabled;
            btOpSub.IsEnabled = isEnabled;
            btOpMul.IsEnabled = isEnabled;
            btOpDiv.IsEnabled = isEnabled;
        }

        private void btNumber_Click(object sender, RoutedEventArgs e)
        {
            TextBox tbCurrent;
            if (calcState == CalcStateEnum.Entering1stValue)
            {
                tbCurrent = tbValue1;
            }
            else if (calcState == CalcStateEnum.Entering2ndValue)
            {
                tbCurrent = tbValue2;
            }
            else
            {
                return; // result is being displayed, number buttons should not work
            }
            //
            Button button = (Button)sender;
            string text = button.Content.ToString();
            switch (text)
            {
                case "+/-":
                    if (tbCurrent.Text == "")
                    {
                        return;
                    }
                    if (tbCurrent.Text[0] == '-') // is first character '-'
                    {
                        string dispText = tbCurrent.Text;
                        tbCurrent.Text = dispText.Substring(1, dispText.Length - 1);
                    } else
                    {
                        tbCurrent.Text = "-" + tbCurrent.Text;
                    }
                    break;
                case ".":
                    tbCurrent.Text += text; // TODO: do better, only one dot allowed
                    break;
                default: // numbers
                    // append this to the correct text both
                    tbCurrent.Text += text;
                    break;
            }
        }

        private void btOperation_Click(object sender, RoutedEventArgs e)
        {
            if (calcState != CalcStateEnum.Entering1stValue)
            {
                MessageBox.Show("Internal error, try reseting the calculator"); // FIXME: nicer
                return;
            }
            // make sure tbValue1 contains a valid floating point
            double val;
            if (!double.TryParse(tbValue1.Text, out val))
            {
                MessageBox.Show("Please enter numerical value 1"); // FIXME: make it nice
                return;
            }
            //
            Button button = (Button)sender;
            string text = button.Content.ToString();
            lblOperation.Content = text;
            calcState = CalcStateEnum.Entering2ndValue; // move to next state
            tbValue1.IsEnabled = false;
            tbValue2.IsEnabled = true;
            btResult.IsEnabled = true;
            Keyboard.Focus(tbValue2); // give input 2 focus.
            setOpButtonsEnabled(false); // disable operation buttons
        }

        private void btResult_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (calcState != CalcStateEnum.Entering2ndValue)
                {
                    MessageBox.Show("Internal error, try reseting the calculator"); // FIXME: nicer
                    return;
                }
                setNumButtonsEnabled(false);
                setOpButtonsEnabled(false);
                tbValue2.IsEnabled = false;
                tbResult.IsEnabled = true;
                double val1 = double.Parse(tbValue1.Text);
                double val2 = double.Parse(tbValue2.Text);
                double result = 0;
                switch (lblOperation.Content)
                {
                    case "+":
                        result = val1 + val2;
                        break;
                    case "-":
                        result = val1 - val2;
                        break;
                    case "*":
                        result = val1 * val2;
                        break;
                    case "/":
                        result = val1 / val2;
                        break;
                }
                tbResult.Text = string.Format("{0:G}", result);
            } catch (FormatException ex)
            {
                MessageBox.Show("Number format exception (internal error)"); // FIXME: nicer
            }
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            resetCalc();
        }

        private void textBoxFloatingPoint_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // FIXME
            /*
            TextBox textBox = (TextBox)sender;
            double val;
            e.Handled = double.TryParse(textBox.Text, out val); */
        }
    }
}
