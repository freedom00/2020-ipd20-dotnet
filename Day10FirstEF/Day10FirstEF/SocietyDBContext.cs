﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class SocietyDBContext : Microsoft.EntityFrameworkCore.DbContext
    {
        virtual public Microsoft.EntityFrameworkCore.DbSet<Person> People { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Person>().Property(d => d.Gender).HasConversion(new EnumToStringConverter<Person.GenderEnum>());
        }
    }
}
