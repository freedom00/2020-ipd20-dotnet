﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {

        static void Main(string[] args)
        {
            Random random = new Random();
            SocietyDBContext ctx = new SocietyDBContext();
            Person p1 = new Person { Name = "Jerry", Age = random.Next(100) , Gender = Person.GenderEnum.Male }; // state: new, detached

            ctx.People.Add(p1); // Insert operation is scheduled here but NOT executed yet
            ctx.SaveChanges(); // sychronize objects in memory with database
            Console.WriteLine("Record added");

            //
            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
