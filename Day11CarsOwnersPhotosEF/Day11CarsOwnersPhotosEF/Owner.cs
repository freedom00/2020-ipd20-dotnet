﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersPhotosEF
{
    class Owner
    {
        [Key]
        public int Id { get; set; }

        [Required] // means not-null
        [StringLength(100)] // nvarchar(100)
        public string Name { get; set; }

        [NotMapped]
        public int CarsNumber { get { return 0; } } // compute only, don't store in DB

        [Column(TypeName = "image")]
        public byte[] Photo { get; set; } // blob
        // ICollection<Car> CarsInGarage; // one to many
    }
}
