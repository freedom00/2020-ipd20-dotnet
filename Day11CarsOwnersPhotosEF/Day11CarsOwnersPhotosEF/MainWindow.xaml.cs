﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersPhotosEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ParkingDBContext ctx;

        public MainWindow()
        {
            try
            {
                ctx = new ParkingDBContext();
                InitializeComponent();
            } catch (SystemException ex)
            {
                MessageBox.Show("Fatal error connecting to database:\n" + ex.Message); // FIXME: nicer
            }
        }

        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }

        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    imgPicture.Source = new BitmapImage(new Uri(openFileDialog.FileName));
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error opening file:\n" + ex.Message); // FIXME: nicer
                }
                catch (UriFormatException ex) {
                    MessageBox.Show("Error opening file:\n" + ex.Message); // FIXME: nicer
                }
            }
                
        }

        private void btAddOwner_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] data = bitmapImageToByteArray((BitmapImage)imgPicture.Source);
                string name = tbName.Text; // TODO: Verify value
                ctx.Owners.Add(new Owner() { Name = name, Photo = data });
                ctx.SaveChanges();
            } catch (SystemException ex)
            {
                MessageBox.Show("Error saving record:\n" + ex.Message); // FIXME: nicer
            }
        }

        private void btUpdateOwner_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btDeleteOwner_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
