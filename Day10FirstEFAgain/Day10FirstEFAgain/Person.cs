﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEFAgain
{
    // [Table("Personalities")]
    class Person
    {
        public enum GenderEnum { Male = 1, Female = 2, NA = 3 }

        // [Key]
        public int Id { get; set; }

        // public int Problem { get; set; }

        [Required] // means not-null
        [StringLength(50)] // nvarchar(50)
        public string Name { get; set; }

        // query: select * from People where Age > 18
        // without index: O(n) - linear search, all records must be scanned
        // with index Log(n) approximately
        [Index] // just to speedup operations, not unique
        public int Age { get; set; }

        // this method can be used both from Age's setter and from UI
        public static bool IsAgeValid(int age)
        {
            return !(age < 0 || age > 150);
        }

        /* // TODO: find out why it did not work
        // sometimes we want null-allowed
        [StringLength(50)] // nvarchar(50)
        [Index(IsUnique = true)] // unique
        public string Email { get; set; }
        */

        [NotMapped] // in memory only, not in database
        public string Comment { get; set; }

        [EnumDataType(typeof(GenderEnum))]
        public GenderEnum Gender { get; set; }
    }
}
