﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEFAgain
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Random random = new Random();
                SocietyDBContextAgain ctx = new SocietyDBContextAgain();

                // equivalent of Insert
                Person p1 = new Person { Name = "Jerry", Age = random.Next(100), Gender = Person.GenderEnum.Male }; // state: new, detached

                ctx.People.Add(p1); // Insert operation is scheduled here but NOT executed yet
                ctx.SaveChanges(); // sychronize objects in memory with database
                Console.WriteLine("Record added");

                // equivalent of update - fetch then modify, save changes
                Person p2 = (from p in ctx.People where p.Id == 1 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {
                    p2.Name = "Alibaba 111111"; // entity framework is watching and notices the modification
                    ctx.SaveChanges(); // update the database to synchronize entities in memory with the database
                    Console.WriteLine("Record updated");
                }
                else
                {
                    Console.WriteLine("record to update not found");
                }
                //

                // delete - fetch then schedule for deletion, then save changes
                Person p3 = (from p in ctx.People where p.Id == 4 select p).FirstOrDefault<Person>();
                if (p3 != null)
                {
                    ctx.People.Remove(p3); // schedule for deletion from database
                    ctx.SaveChanges();
                    Console.WriteLine("Record deleted");
                }
                else
                {
                    Console.WriteLine("record to delete not found");
                }
                //
                // print all records
                var peopleCol = from p in ctx.People select p; // equiv: SELECT * FROM People
                foreach (Person p in peopleCol)
                {
                    Console.WriteLine($"{p.Id}: {p.Name} is {p.Age} y/o, {p.Gender}");
                }
            }
            catch (ArgumentException ex)
            {
                // special handler in case setters / constructor of our entity throws AE
                // and you want to perform a different action in such case
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine("Database operation failed: " + ex.Message);
            }
            finally
            {
                //
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }

        }
    }
}
