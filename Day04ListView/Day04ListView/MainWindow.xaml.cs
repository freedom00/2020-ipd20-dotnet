﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day04ListView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Person> peopleList = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();
            //lvPeople.DataContext = peopleList;
            lvPeople.ItemsSource = peopleList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            int age;
            if (!int.TryParse(tbAge.Text, out age))
            {
                MessageBox.Show(this, "Please insert a valid age. Age should be a number.", "Input Data Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            tbName.Text = "";
            tbAge.Text = "";
            Person person = new Person(name, age);
            peopleList.Add(person);
            lvPeople.Items.Refresh();
            //lvPeople.Items.Add(person); // using Data Context only
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPerson == null)
            {
                return;
            }
            // after we modify an item in peopleList directory we must call
            string name = tbName.Text;
            int age;
            if (!int.TryParse(tbAge.Text, out age))
            {
                MessageBox.Show(this, "Please insert a valid age. Age should be a number.", "Input Data Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            selectedPerson.Name = name;
            selectedPerson.Age = age;
            lvPeople.Items.Refresh(); // let ListView know that the underlying data has changed
        }

        Person selectedPerson;

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedPerson = (Person)lvPeople.SelectedItem;
            if (selectedPerson != null)
            {
                tbName.Text = selectedPerson.Name;
                tbAge.Text = selectedPerson.Age + "";
            } else
            {
                // clean up or not ?
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPerson == null)
            {
                return;
            }
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete this item?\n" + selectedPerson, "Delete confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                peopleList.Remove(selectedPerson);
                lvPeople.Items.Refresh();
            }
        }
    }
}
