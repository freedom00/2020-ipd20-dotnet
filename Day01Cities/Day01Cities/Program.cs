﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{


    class City
    {
        public string Name;
        public double PopulationMillions;
        public override string ToString()
        {
            return string.Format("City: {0} with {1} mil. population", Name, PopulationMillions);
        }
    }

    class BetterCity
    {
        public BetterCity(string name)
        {
            Name2 = name; // calls the setter
        }

        public string Name { get; set; }

        private string _name2;
        public string Name2
        {
            get
            {
                return _name2;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new InvalidOperationException("Name length must be 2 characters or more");
                }
                _name2 = value;
            }
        }
        public override string ToString()
        {
            return string.Format("City: {0}", Name2);
        }

    }

    class Program
    {
        static public List<City> GlobalCitiesList = new List<City>();

        static void Main(string[] args)
        {
            BetterCity bc1 = new BetterCity("???");
            bc1.Name2 = "Montreal";
            // bc1._name2 = "aaa";
            bc1.Name2 = "a"; // throws exception

            City c1 = new City { Name = "Montreal", PopulationMillions = 3.5 };
            Console.WriteLine("c1 is: " + c1);
            List<City> citiesList = new List<City>();
            citiesList.Add(c1);

            //
            Console.WriteLine("Press Any Key to finish.");
            Console.ReadKey();
        }
    }
}
