﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05TravelList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Trip> tripsList = new List<Trip>();

        public MainWindow()
        {
            InitializeComponent();
            lvTrips.ItemsSource = tripsList;
        }

        private void btAddTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string dest = tbDest.Text;
                string name = tbName.Text;
                string passNo = tbPassNo.Text;

                if (!dpDeparture.SelectedDate.HasValue)
                {
                    MessageBox.Show(this, "Departure date must be selected.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                DateTime depDate = dpDeparture.SelectedDate.Value;

                if (!dpReturn.SelectedDate.HasValue)
                {
                    MessageBox.Show(this, "Return date must be selected.", "Input Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                DateTime retDate = dpReturn.SelectedDate.Value;
                Trip trip = new Trip(dest, name, passNo, depDate, retDate);
                tripsList.Add(trip);
                lvTrips.Items.Refresh();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this, ex.Message, "Input Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
