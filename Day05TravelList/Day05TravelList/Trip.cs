﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05TravelList
{
    class Trip
    {
        public Trip(string destination, string name, string passport, DateTime depDate, DateTime retDate)
        {
            Destination = destination;
            TravellerName = name;
            TravellerPassportNo = passport;
            setDepartureAndReturnDates(depDate, retDate);
        }
        public string Destination { get; set; } // 2-30 characters, no semicolons
        public string TravellerName { get; set; } // 2-30 characters, no semicolons
        public string TravellerPassportNo { get; set; } // two uppercase letters followed by 8 digts

        private DateTime _depDate, _retDate;
        public DateTime DepartureDate
        {
            get
            {
                return _depDate;
            }
        } // year 1900-2100, departure before return
        public DateTime ReturnDate
        {
            get
            {
                return _retDate;
            }
        }

        public void setDepartureAndReturnDates(DateTime depDate, DateTime retDate)
        {
            if (depDate > retDate)
            {
                throw new ArgumentException("Departure must take place before return");
            }
            if (depDate.Year < 1900 || depDate.Year > 2100 || retDate.Year < 1900 || retDate.Year > 2100)
            {
                throw new ArgumentException("Dates must be between year 1900 and 2100");
            }
            _depDate = depDate;
            _retDate = retDate;
        }
    }
}
