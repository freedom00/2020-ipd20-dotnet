﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19Explanations
{
    class A
    {
        public void M1()
        {
            Console.WriteLine("A.m1");
        }

        virtual public void M2()
        {
            Console.WriteLine("A.m2");
        }

    }

    class B : A
    {
        new public void M1()
        {
            Console.WriteLine("B.m1");
        }

        override public void M2()
        {
            Console.WriteLine("B.m2");
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            A a = new A();
            B b = new B();
            Console.WriteLine("Non-virtual calls:");
            a.M1();
            b.M1();
            Console.WriteLine("A type but really B calling M1:");
            A reallyB = b;
            reallyB.M1();

            Console.WriteLine("\nVirtual calls:");
            a.M2();
            b.M2();
            Console.WriteLine("A type but really B calling M1:");
            A reallyB__ = b;
            reallyB__.M2(); // virtual call

            Console.ReadKey();
        }
    }
}
