﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07SimpleDatabase
{
    class Program
    {
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Teacher\Documents\2020-ipd20-dotnet\Day07SimpleDatabase\SimpleDB.mdf;Integrated Security=True;Connect Timeout=30";

        static void Main(string[] args)
        {
            // FIXME: in real program be prepared to handle SqlException
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            // insert
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People (Name, Age) VALUES (@Name, @Age)", conn);
            cmdInsert.Parameters.AddWithValue("@Name", "JerryAAAA");
            cmdInsert.Parameters.AddWithValue("@Age", 43);
            cmdInsert.ExecuteNonQuery();
            
            // select
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM People", conn);
            SqlDataReader selectReader = cmdSelectAll.ExecuteReader();
            while (selectReader.Read()) 
            {
                int id = selectReader.GetInt32(0);
                string name = selectReader.GetString(1);
                int age = selectReader.GetInt32(2);
                Console.WriteLine("{0}: {1} is {2} y/o", id, name, age);
            }

            Console.WriteLine("Press any key");
            Console.ReadKey();

        }
    }
}
