﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {

        static void ReadDataFromFile()
        {
            // It is okay to read all data at once if the number of lines is reasonably small,
            // e.g. less than 10,000. Otherise use a loop to read one line at a time.
            try
            {
                string[] linesArray = File.ReadAllLines(@"..\..\people.txt"); // FIXME ex !!!
                foreach (string line in linesArray)
                {
                    try
                    {
                        string type = line.Split(';')[0];
                        switch (type)
                        {
                            case "Person":
                                Person p = new Person(line);
                                peopleList.Add(p);
                                break;
                            case "Teacher":
                                Teacher t = new Teacher(line);
                                peopleList.Add(t);
                                break;
                            case "Student":
                                Student s = new Student(line);
                                peopleList.Add(s);
                                break;
                            default:
                                break;
                        }
                    }
                    catch (InvalidParameterException ex)
                    {
                        Console.WriteLine("Error parsing line `{0}`:\n{1}\nskipping.", line, ex.Message);
                    }
                }
            } catch (IOException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            } catch (SystemException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }

        }

        static List<Person> peopleList = new List<Person>();


        static void PrintObjCreationInfo(string why)
        {
            Console.WriteLine("Event Object Creation: " + why);
        }

        static void PrintFANCYObjCreation(string why)
        {
            File.AppendAllText(@"..\..\eventlog.txt", "ObjCreated: " + why + "\n");
        }

        static void Main(string[] args)
        {

            // GeoCoordinate gc1;

            int [,] a2int = new int[2,3]; // rectangular array
            int[][] a2jagged = new int[][] { new int[]{ 1 }, new int[] { 2, 3 }, new int[] { 4, 5, 6 } };

            a2int[1, 2] = 7;
            a2jagged[1][2] = 13;


            Person.HandlerForObjCreation = PrintObjCreationInfo; // reference to method
            Person.HandlerForObjCreation += PrintFANCYObjCreation; // reference to method
            // Person.HandlerForObjCreation -= PrintObjCreationInfo; // reference to method

            ReadDataFromFile();
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p.ToDataString()); // must be polymorphic call - using virtual/override
            }

            // only students
            var onlyStudents = from p in peopleList where p is Student select (Student)p;
            // ordering people by name
            var peopleOrderedByName = from p in peopleList orderby p.Name select p;
            //
            var peopleNamesAgesInOrder = from p in peopleList orderby p.Age select p.Name;
            //
            var peopleUnderage = from p in peopleList where p.Age < 18 select p;
            //
            var peopleUnderageAsTuples = from p in peopleList where p.Age < 18 select new Tuple<string,int>(p.Name, p.Age);
            //
            var sumOfAges = (from p in peopleList select p.Age).Sum();
            //
            var AverageOfAges = (from p in peopleList select p.Age).Average(); // make sure double is returned
            //
            List<string> linesToWrite = new List<string>();
            foreach (Person p in peopleOrderedByName)
            {
                linesToWrite.Add(p.ToString());
            }
            // TODO: Write lines to file

            // collection to list
            List<Person> listOfPeopleOrderedByName = new List<Person>(peopleOrderedByName);


            Console.WriteLine("Press any key to finish");
            Console.ReadKey();
        }
    }
}
