﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{

    public class InvalidParameterException : Exception
    {
        public InvalidParameterException(string msg) : base(msg) { }
    }

    public class Person
    {
        public delegate void ObjectCreatedDelegate(string msg); // defines type
        public static ObjectCreatedDelegate HandlerForObjCreation; // list of reference to method

        protected Person()
        {
            if (HandlerForObjCreation != null)
            {
                HandlerForObjCreation("Object is being created from default constructor");
            }
        } // only accessible within Person and child-classes

        public Person(string name, int age)
        {
            if (HandlerForObjCreation != null)
            {
                HandlerForObjCreation("Object is being created from name/age constructor");
            }
            Name = name;
            Age = age;
        }

        public Person(string dataLine) {
            if (HandlerForObjCreation != null)
            {
                HandlerForObjCreation("Object is being created from data line");
            }
            string[] data = dataLine.Split(';');
            if (data.Length != 3)
            {
                throw new InvalidParameterException("Person data line must have 3 fields");
            }
            if (data[0] != "Person")
            {
                throw new InvalidParameterException("Data line does not describe Person");
            }
            Name = data[1];
            int age; // out parameter cannot be a property
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
        }

        private string _name;
        public string Name
        { // 1-50 characters, no semicolons
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidParameterException("Name must be 1-50 characters, no semicolons");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        { // 0-150
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidParameterException("Age must be 0-150");
                }
                _age = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Person: {0} is {1} y/o", Name, Age);
        }

        public virtual string ToDataString()
        {
            return string.Format("Person;{0};{1}", Name, Age);
        }

    }

    public class Teacher : Person
    {
        public Teacher(string name, int age, string subject, int yoe) : base (name, age)
        {
            Subject = subject;
            YearsOfExperience = yoe;
        }
        public Teacher(string dataLine) : base()
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterException("Teacher data line must have 5 fields");
            }
            if (data[0] != "Teacher")
            {
                throw new InvalidParameterException("Data line does not describe Teacher");
            }
            // some unfortunate code duplication here, we could remove it but not sure it's worth it
            Name = data[1];
            int age; // out parameter cannot be a property
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
            // parse the rest of fields
            Subject = data[3];
            int yoe;
            if (!int.TryParse(data[4], out yoe))
            {
                throw new InvalidParameterException("Years of experience in data line must be an integer");
            }
            YearsOfExperience = yoe;
        }
        private string _subject;
        public string Subject
        { // 1-50 characters, no semicolons
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidParameterException("Subject must be 1-50 characters, no semicolons");
                }
                _subject = value;
            }
        }
        private int _yearsOfExperience;
        public int YearsOfExperience
        { // 0-100
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new InvalidParameterException("Years of experience must be 0-100");
                }
                _yearsOfExperience = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Teacher: {0} is {1} y/o, teaching {2} since {3} years", Name, Age, Subject, YearsOfExperience);
        }
        public override string ToDataString()
        {
            return string.Format("Teacher;{0};{1};{2};{3}", Name, Age, Subject, YearsOfExperience);
        }
    }

    public class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base()
        {
            Name = name;
            Age = age;
            Program = program;
            GPA = gpa;
        }
        public Student(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidParameterException("Student data line must have 5 fields");
            }
            if (data[0] != "Student")
            {
                throw new InvalidParameterException("Data line does not describe Teacher");
            }
            // some unfortunate code duplication here, we could remove it but not sure it's worth it
            Name = data[1];
            int age; // out parameter cannot be a property
            if (!int.TryParse(data[2], out age))
            {
                throw new InvalidParameterException("Age in data line must be an integer");
            }
            Age = age;
            // parse the rest of fields
            Program = data[3];
            double gpa;
            if (!double.TryParse(data[4], out gpa))
            {
                throw new InvalidParameterException("GPA in data line must be numerical");
            }
            GPA = gpa;
        }
        private string _program; 
        public string Program
        { // 1-50 characters, no semicolons
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidParameterException("Program must be 1-50 characters, no semicolons");
                }
                _program = value;
            }
        }
        private double _gpa;
        public double GPA
        { // 0-4.3
            get
            {
                return _gpa;
            }
            set
            {
                if (value < 0 || value > 4.3)
                {
                    throw new InvalidParameterException("GPA must be 0-4.3");
                }
                _gpa = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Student: {0} is {1} y/o, studying {2}, has {3} GPA", Name, Age, Program, GPA);
        }
        public override string ToDataString()
        {
            return string.Format("Student;{0};{1};{2};{3}", Name, Age, Program, GPA);
        }
    }
}



