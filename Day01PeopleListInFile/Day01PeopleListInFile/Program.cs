﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    class Program
    {
        static void AddPersonInfo()
        {

        }
        static void ListAllPersonsInfo()
        {
        }
        static void FindPersonByName()
        {

        }
        static void FindPersonYoungerThan()
        {

        }

        static void LoadDataFromFile()
        {

        }

        static void SaveDataToFile()
        {
            try
            {
                List<String> linesList = new List<String>();
                foreach (Person p in peopleList)
                {
                    linesList.Add(String.Format("{0};{1};{2}", p.Name, p.Age, p.City));
                }
                File.WriteAllLines("people.txt", linesList); // ex
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
            
            /* // Useful when handling 3 or more exceptions the same way
             catch (Exception ex)
            {
                if (ex is IOException || ex is SystemException)
                {
                    Console.WriteLine("Error writing file: " + ex.Message);
                }
                else
                {
                    throw ex; // continue propagation
                }
            } */
        }

        static int getMenuChoice()
        {
            while (true)
            {
                Console.Write(
    @"1. Add person info
2. List persons info
3. Find a person by name
4. Find all persons younger than age
0. Exit
Enter your choice: ");
                int choice;
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                if (choice < 0 || choice > 4)
                {
                    Console.WriteLine("Invalid choice");
                    continue;
                }
                return choice;
            }
        }

        static List<Person> peopleList = new List<Person>();

        static void Main(string[] args)
        {
            LoadDataFromFile();
            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveDataToFile();
                        Console.WriteLine("Data saved. Exiting.");
                        return; // terminate the program
                    default:
                        Console.WriteLine("Internal error: invalid control flow in menu");
                        break;
                }
            }
        }
    }
}
