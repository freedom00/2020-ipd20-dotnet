﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person // : Object
    {
        public Person(string name, int age, string city) // : base()
        {
            Name = name;
            Age = age;
            City = city;
        }

        private string _name;
        public string Name {
            // Name 2-100 characters long, not containing semicolons
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("Name must be 2-100 characters long, not containing semicolons");
                }
                _name = value;
            }
        }

        private int _age;
        public int Age
        { // Age 0-150
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("Age must be 0-150");
                }
                _age = value;
            }
        }

        private string _city;
        public string City
        { // City 2-100 characters long, not containing semicolons
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100 || value.Contains(";"))
                {
                    throw new ArgumentException("City must be 2-100 characters long, not containing semicolons");
                }
                _city = value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} is {1} y/o lives in {2}", Name, Age, City);
        }

    }
}
