﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day05DataInFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void miFileNew_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void miFileOpen_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "Error reading file:\n" + ex.Message, "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void miFileSave_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void miFileSaveAs_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    File.WriteAllText(saveFileDialog.FileName, txtEditor.Text);
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "Error writing file:\n" + ex.Message, "File access error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }

        private void miFileExit_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void txtEditor_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
